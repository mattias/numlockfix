﻿namespace Laserbrain
{
	using System;
	using System.Drawing;
	using System.Windows.Forms;

	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();
			UpdateLabel();
		}

		private void UpdateLabel()
		{
			if (NumLockSupport.NumLock)
			{
				label1.ForeColor = Color.Firebrick;
				label1.Text = "NumLock är PÅ";
			}
			else
			{
				label1.ForeColor = Color.ForestGreen;
				label1.Text = "NumLock är AV";
			}
		}

		private void ButtonClick(object sender, EventArgs e)
		{
			NumLockSupport.NumLock = !NumLockSupport.NumLock;
			Application.DoEvents();
			UpdateLabel();
		}

		private void TimerTick(object sender, EventArgs e)
		{
			UpdateLabel();
		}
	}
}