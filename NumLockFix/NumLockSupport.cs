﻿namespace Laserbrain
{
	using System.Runtime.InteropServices;

	public static class NumLockSupport
	{
		private const int VkNumlock = 0x90;
		private const int KeyeventfExtendedkey = 0x0001;
		private const int KeyeventfKeyup = 0x0002;

		[DllImport("user32.dll", EntryPoint = "keybd_event", CharSet = CharSet.Auto, ExactSpelling = true)]
		private static extern void keybd_event(byte vk, byte scan, int flags, int extrainfo);

		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		private static extern int GetKeyboardState(byte[] keystate);

		public static bool NumLock
		{
			get
			{
				return GetNumLock();
			}
			set
			{
				SetNumLock(value);
			}
		}

		private static bool GetNumLock()
		{
			var keyState = new byte[256];

			GetKeyboardState(keyState);
			var currentState = keyState[VkNumlock] != 0;

			return currentState;
		}

		private static void SetNumLock(bool wantedState)
		{
			if (wantedState == GetNumLock())
			{
				return;
			}

			keybd_event(
				VkNumlock,
				0x45,
				KeyeventfExtendedkey | 0,
				0);

			keybd_event(
				VkNumlock,
				0x45,
				KeyeventfExtendedkey | KeyeventfKeyup,
				0);
		}
	}
}